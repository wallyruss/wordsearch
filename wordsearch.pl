#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Long;
use Cwd 'abs_path';

# Default path for dictionary
my $script_path = abs_path($0);
(my $root_dir = $script_path) =~ s/\/\Q$0\E//;
my $dict_path = "$root_dir/dict/american-english";

my $auto;
GetOptions(
  'a|auto' => \$auto,
  'd|dictionary=s' => \$dict_path
);

# Read input from command line, check for errors
my $input_file = shift;
die "You must specify a text file containing the input word search.\n" if (!defined($input_file));
die "Could not locate input file: '$input_file'.\n" if (!-e $input_file);
die "Could not locate dictionary file: '$dict_path'.\n" if (!-e $dict_path && $auto);

# Parse input 
open(my $input, "<", $input_file) || die "Could not open '$input_file': $!.\n";
chomp(my @input =  <$input>);
close($input);

# Determine character locations
my @wordsearch;
my %char_locations;
foreach my $y (0 .. $#input) {
  my $line = lc($input[$y]);

  my @chars = split("", $line);
  foreach my $x (0 .. $#chars) {
    my $char = $chars[$x];  
      
    $wordsearch[$y][$x] = $char;
    push(@{$char_locations{$char}}, "$x,$y");
  }
}

# Load potential words into dictionary
my @dictionary = load_dictionary(3, 10) if ($auto);

# Clone for fancy output with colors
my @colored_wordsearch = @{clone(\@wordsearch)};

# Wordsearch dimensions
my $max_y = scalar(@wordsearch) - 1;
my $max_x = scalar(@{$wordsearch[0]}) - 1;

output_wordsearch();

# Main loop
if ($auto) {

  # For storing found hits
  my %finds;
  my %dir_decoder = (
    1 => "up",
    2 => "left",
    3 => "down",
    4 => "right",
    5 => "up and left",
    6 => "up and right",
    7 => "down and left",
    8 => "down and right"
  );

  # Iterate through each word in our dictionary
  foreach my $word (@dictionary) {
    next if ($word !~ /^\w+$/);
    chomp($word); 
    $word = lc($word);

    # Check all possible locations and directions for word
    my $found = 0;
    my @start_locations = @{$char_locations{substr($word, 0, 1)}};
    foreach my $start_location (@start_locations) {
      $found = check_all($word, $start_location);

      # Store found word
      if ($found) {
        $finds{$word} = { 
          "dir"      => $found, 
          "location" => $start_location
        };
        last;
      }
    }
  }

  # Write hits to an output file
  my $output_file = "$input_file.found";
  open(my $output, ">", $output_file) || die "Could not open '$output_file': $!.\n";

    # Iterate through finds sorted by descending length
    foreach my $word (sort { length($b) <=> length($a) } keys %finds) {
      my $location  = $finds{$word}->{location};
      my $direction = $dir_decoder{$finds{$word}->{dir}};
      print {$output} "'$word' found at ($location) going $direction\n";
    }
  close($output);
}
else {

  # Prompt user for words to search for 
  print "Input word to search for: ";
  while (my $word = <STDIN>) {
    next if ($word !~ /^\w+$/);
    chomp($word); 
    $word = lc($word);

    # Check all possible locations and directions for word
    my $found = 0;
    my @start_locations = @{$char_locations{substr($word, 0, 1)}};
    foreach my $start_location (@start_locations) {
      $found = check_all($word, $start_location);
      last if ($found);
    }
    print "Word not found.\n\n" if (!$found);
    print "Input word to search for: ";
  }
}

# For fancy output
sub output_wordsearch {
  foreach my $y (@colored_wordsearch) {
    print "@{$y}\n"
  }
  print "\n";

  @colored_wordsearch = @{clone(\@wordsearch)};
}

# Replacement subroutine for Clone 'clone'
sub clone {
  my ($input) = @_;
  my @input = @{$input};

  my @output;
  foreach my $y (0 .. $#input) {
    foreach my $x (0 .. scalar(@{$input[$y]}) - 1) {
      $output[$y][$x] = $input[$y][$x];
    }
  }

  return \@output;
}

# Checks all possible directions for a given word and location
sub check_all {
  my ($word, $start_location) = @_; 

  return 1 if (check_up($word, $start_location));
  return 2 if (check_left($word, $start_location));
  return 3 if (check_down($word, $start_location));
  return 4 if (check_right($word, $start_location));
  return 5 if (check_up_left($word, $start_location));
  return 6 if (check_up_right($word, $start_location));
  return 7 if (check_down_left($word, $start_location));
  return 8 if (check_down_right($word, $start_location));
}

# Checks up from a location for a given word
sub check_up {
  my ($word, $start_location) = @_; 

  my ($x, $y) = split(",", $start_location);

  # Check if too long to fit
  if ($y - length($word) + 1 < 0) {
    return '';
  }

  # Move in desired direction
  my @chars = split("", $word);
  foreach my $offset (0 .. $#chars) {
    my $char = $chars[$offset];     

    my $x = $x;
    my $y = $y - $offset; 
    if ($wordsearch[$y][$x] ne $char) {
      @colored_wordsearch = @{clone(\@wordsearch)};
      return;
    }
    else {
      $colored_wordsearch[$y][$x] = "\e[43m$colored_wordsearch[$y][$x]\e[0m";
    }
  }

  output_wordsearch();
  print "'$word' found at $x,$y moving up\n\n";
  return "$x,$y";
}

# Checks left from a location for a given word
sub check_left {
  my ($word, $start_location) = @_; 

  my ($x, $y) = split(",", $start_location);

  # Check if too long to fit
  if ($x - length($word) + 1 < 0) {
    return '';
  }

  # Move in desired direction
  my @chars = split("", $word);
  foreach my $offset (0 .. $#chars) {
    my $char = $chars[$offset];     

    my $x = $x - $offset;
    my $y = $y; 
    if ($wordsearch[$y][$x] ne $char) {
      @colored_wordsearch = @{clone(\@wordsearch)};
      return;
    }
    else {
      $colored_wordsearch[$y][$x] = "\e[43m$colored_wordsearch[$y][$x]\e[0m";
    }
  }

  output_wordsearch();
  print "'$word' found at $x,$y moving left\n\n";
  return "$x,$y";
}

# Checks down from a location for a given word
sub check_down {
  my ($word, $start_location) = @_; 

  my ($x, $y) = split(",", $start_location);

  # Check if too long to fit
  if ($y + length($word) - 1 > $max_y) {
    return '';
  }

  # Move in desired direction
  my @chars = split("", $word);
  foreach my $offset (0 .. $#chars) {
    my $char = $chars[$offset];     

    my $x = $x;
    my $y = $y + $offset; 
    if ($wordsearch[$y][$x] ne $char) {
      @colored_wordsearch = @{clone(\@wordsearch)};
      return;
    }
    else {
      $colored_wordsearch[$y][$x] = "\e[43m$colored_wordsearch[$y][$x]\e[0m";
    }
  }

  output_wordsearch();
  print "'$word' found at $x,$y moving down\n\n";
  return "$x,$y";
}

# Checks right from a location for a given word
sub check_right {
  my ($word, $start_location) = @_; 

  my ($x, $y) = split(",", $start_location);

  # Check if too long to fit
  if ($x + length($word) - 1 > $max_x) {
    return '';
  }

  # Move in desired direction
  my @chars = split("", $word);
  foreach my $offset (0 .. $#chars) {
    my $char = $chars[$offset];     

    my $x = $x + $offset;
    my $y = $y; 
    if ($wordsearch[$y][$x] ne $char) {
      @colored_wordsearch = @{clone(\@wordsearch)};
      return;
    }
    else {
      $colored_wordsearch[$y][$x] = "\e[43m$colored_wordsearch[$y][$x]\e[0m";
    }
  }

  output_wordsearch();
  print "'$word' found at $x,$y moving right\n\n";
  return "$x,$y";
}

# Checks up-left diagonal of a location for a given word
sub check_up_left {
  my ($word, $start_location) = @_; 

  my ($x, $y) = split(",", $start_location);

  # Check if too long to fit
  if ($x - length($word) + 1 < 0 || $y - length($word) + 1 < 0) {
    return '';
  }

  # Move in desired direction
  my @chars = split("", $word);
  foreach my $offset (0 .. $#chars) {
    my $char = $chars[$offset];     

    my $x = $x - $offset;
    my $y = $y - $offset; 
    if ($wordsearch[$y][$x] ne $char) {
      @colored_wordsearch = @{clone(\@wordsearch)};
      return;
    }
    else {
      $colored_wordsearch[$y][$x] = "\e[43m$colored_wordsearch[$y][$x]\e[0m";
    }
  }

  output_wordsearch();
  print "'$word' found at $x,$y moving up left\n\n";
  return "$x,$y";
}

# Checks up-right diagonal of a location for a given word
sub check_up_right {
  my ($word, $start_location) = @_; 

  my ($x, $y) = split(",", $start_location);

  # Check if too long to fit
  if ($x + length($word) - 1 > $max_x || $y - length($word) + 1 < 0) {
    return '';
  }

  # Move in desired direction
  my @chars = split("", $word);
  foreach my $offset (0 .. $#chars) {
    my $char = $chars[$offset];     

    my $x = $x + $offset;
    my $y = $y - $offset; 
    if ($wordsearch[$y][$x] ne $char) {
      @colored_wordsearch = @{clone(\@wordsearch)};
      return;
    }
    else {
      $colored_wordsearch[$y][$x] = "\e[43m$colored_wordsearch[$y][$x]\e[0m";
    }
  }

  output_wordsearch();
  print "'$word' found at $x,$y moving up right\n\n";
  return "$x,$y";
}

# Checks down-left diagonal of a location for a given word
sub check_down_left {
  my ($word, $start_location) = @_; 

  my ($x, $y) = split(",", $start_location);

  # Check if too long to fit
  if ($x - length($word) + 1 < 0 || $y + length($word) - 1 > $max_y) {
    return '';
  }

  # Move in desired direction
  my @chars = split("", $word);
  foreach my $offset (0 .. $#chars) {
    my $char = $chars[$offset];     

    my $x = $x - $offset;
    my $y = $y + $offset; 
    if ($wordsearch[$y][$x] ne $char) {
      @colored_wordsearch = @{clone(\@wordsearch)};
      return;
    }
    else {
      $colored_wordsearch[$y][$x] = "\e[43m$colored_wordsearch[$y][$x]\e[0m";
    }
  }

  output_wordsearch();
  print "'$word' found at $x,$y moving down left\n\n";
  return "$x,$y";
}

# Checks down-right diagonal of a location for a given word
sub check_down_right {
  my ($word, $start_location) = @_; 

  my ($x, $y) = split(",", $start_location);

  # Check if too long to fit
  if ($x + length($word) - 1 > $max_x || $y + length($word) - 1 > $max_y) {
    return '';
  }

  # Move in desired direction
  my @chars = split("", $word);
  foreach my $offset (0 .. $#chars) {
    my $char = $chars[$offset];     

    my $x = $x + $offset;
    my $y = $y + $offset; 
    if ($wordsearch[$y][$x] ne $char) {
      @colored_wordsearch = @{clone(\@wordsearch)};
      return;
    }
    else {
      $colored_wordsearch[$y][$x] = "\e[43m$colored_wordsearch[$y][$x]\e[0m";
    }
  }

  output_wordsearch();
  print "'$word' found at $x,$y moving down right\n\n";
  return "$x,$y";
}


sub load_dictionary {
  my ($min_length, $max_length) = @_;
  die "Unspecified or invalid \$max_length.\n" if (!defined($max_length));
  die "Unspecified or invalid \$min_length.\n" if (!defined($min_length) || $min_length <= 1);
  die "\$min_length is greater than \$max_length.\n" if ($min_length > $max_length);

  # Unbuffered output
  $|++;

  # Load dictionary into memory
  print "\nLoading dictionary in '$dict_path'... ";
  open(my $dict, "<", $dict_path);
  chomp(my @lines = <$dict>);
  close($dict);
  print "done.\n";

  # Filter words from dictionary
  print "Reducing dictionary to contain only allowed words... ";
  foreach my $line_index (reverse(0 .. $#lines)) {
    my $line = lc($lines[$line_index]);

    # Remove words which are too short or long
    if (length($line) < $min_length || length($line) > $max_length) {
      splice(@lines, $line_index, 1)
    }
    else {
      foreach my $char_index (0 .. length($line) - 1) {

        # Remove words which don't contain the characters we want
        my $char = substr($line, $char_index, 1); 
        if (!exists($char_locations{$char})) {
          splice(@lines, $line_index, 1);
          last;
        }
      }
    }
  }
  print "done (", scalar(@lines), " total words).\n\n";

  $|--;

  return @lines;
}
